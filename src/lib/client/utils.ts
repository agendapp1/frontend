import { tick } from "svelte";

export async function fetchBack(url : string, method : "GET" | "POST" | "PATCH" | "PUT" | "DELETE", body : any, headers? : {[key : string] : "string"}){
	const response = await fetch(url, {
		method,
		credentials: "include",
		headers: {
			"Content-Type": "application/json",
			"Accept": "*/*",
			...(headers ?? {})
		},
		body : body ? JSON.stringify(body) : undefined,
	});
	const data = await response.json();
	return {status : response.status, ...(response.status == 200 ? {data} : {error : data.message})};
}
